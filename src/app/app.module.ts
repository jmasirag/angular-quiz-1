import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { BannerComponent } from './home-page/banner/banner.component';
import { PopularDestinationComponent } from './home-page/popular-destination/popular-destination.component';
import { FeaturedTravelComponent } from './home-page/featured-travel/featured-travel.component';
import { WhyUsComponent } from './home-page/why-us/why-us.component';
import { TestimonialComponent } from './home-page/testimonial/testimonial.component';
import { SideMenuComponent } from './main/side-menu/side-menu.component';
import { FooterComponent } from './main/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    BannerComponent,
    PopularDestinationComponent,
    FeaturedTravelComponent,
    WhyUsComponent,
    TestimonialComponent,
    SideMenuComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
